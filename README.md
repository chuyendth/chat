Installation
Clone this repository
Go inside the directory
Now install composer

~~~
composer install
~~~

Generate key on .env file

~~~
cp .env.example .env
php artisan key:generate
~~~

Migration and seed DB

~~~
php artisan migrate
php artisan db:seed
~~~