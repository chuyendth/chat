import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'

Vue.use(VueRouter)
Vue.use(Vuetify)

import Login from "../components/login/Login"

const routes = [
  { path: '/login', component: Login }
]

const router = new VueRouter({
  routes, // short for `routes: routes`
  hashbang: false,
  mode: 'history'
})

const app = new Vue({
  router
}).$mount('#app')

export default router
